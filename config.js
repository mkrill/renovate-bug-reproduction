// See also https://github.com/renovatebot/docker-renovate/blob/HEAD/docs/gitlab.md#renovate-config
module.exports = {
    baseDir: process.env.$CI_PROJECT_DIR + "/renovate",
    branchPrefix: "renovate/", // default value hardcoded due to potential pipeline rule dependency
    commitMessageLowerCase: "never",
    extends: [
        "config:best-practices", // Preset with best practices from the Renovate maintainers.
        ":rebaseStalePrs", // Rebase existing PRs any time the base branch has been updated.
        ":enableVulnerabilityAlertsWithLabel('security')", // Raise PR when vulnerability alerts are detected with label 'security'
        "mergeConfidence:all-badges" // Show all Merge Confidence badges for pull requests.
    ],
    ignorePrAuthor: true,
    labels: ["renovate"],
    onboardingPrTitle: "ci: DIGIHUB-123456 configure Renovate",
    onboardingCommitMessage: "DIGIHUB-123456 configure Renovate",
    onboardingConfig: {
        "description": "This is an initial onboarding config. If settings should not fit to the repositories needs, feel free to change them.",
        "extends": [
            ":approveMajorUpdates", // Require dependency dashboard approval for major updates.
            ":separateMultipleMajorReleases", // Separate each major version of dependencies into individual branches/PRs.
            ":prConcurrentLimit10" // Limit to maximum 10 open PRs at any time.
        ],
        "automergeSchedule": [
            "at any time"
        ],
        "schedule": [
            "at any time"
        ],
        "timezone": "Europe/Berlin"
    },
    optimizeForDisabled: true,
    platform: "gitlab",
    repositoryCache: "enabled", // Renovate will maintain a JSON file cache per-repository to speed up extractions.
    semanticCommits: "enabled" // Use semantic prefixes for commit messages and PR titles.
}
